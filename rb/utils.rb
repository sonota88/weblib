module Utils
  def self.print_stderr(s, label: nil)
    $stderr.puts "  _____[ #{label} ]____"
    s.each_line { |line|
      $stderr.puts "  | " + line
    }
    $stderr.puts "  ~~~~~[ #{label} ]~~~~"
  end

  def self.puts_e(*args)
    lines = []
    args.each { |arg|
      lines << arg.to_s
    }
    print_stderr(
      lines.join("\n"),
      label: "puts_e"
    )
  end

  def self.p_e(*args)
    lines = []
    args.each { |arg|
      lines << arg.inspect
    }
    print_stderr(
      lines.join("\n"),
      label: "p_e"
    )
  end

  def self.pp_e(*args)
    lines = []
    args.each { |arg|
      lines << arg.pretty_inspect
    }
    print_stderr(
      lines.join("\n"),
      label: "pp_e"
    )
  end
end
