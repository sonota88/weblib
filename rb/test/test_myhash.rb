require_relative "helper"

require "myhash"

class TestMyhash < Minitest::Test
  def test_01
    h =
      {
        "aa" => [
          [7, 8], # array in array
          { :bbCc => 1 }, # obj in array
          { :ddEe => [
              { :ffGg => 2 },
              { :hhIi => 3 }
            ]
          }
        ],
        :jjKk => {
          :llMm => 456, # obj in obj
          :nnOo => [ # array in obj
            { :ppQq => 9 }
          ]
        }
      }

    act =
      Myhash.new(h)
        .to_snake
        .to_sym_key
        .to_plain

    exp =
      {
        :aa => [
          [7, 8], # array in array
          { :bb_cc => 1 }, # obj in array
          { :dd_ee => [
              { :ff_gg => 2 },
              { :hh_ii => 3 }
            ]
          }
        ],
        :jj_kk => {
          :ll_mm => 456, # obj in obj
          :nn_oo => [ # array in obj
            { :pp_qq => 9 }
          ]
        }
      }

    assert_equal(exp, act)
  end
end
