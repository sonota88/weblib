(()=>{

const puts = (...args) => console.log(...args);
const _puts = () => void 0;
const tojson = x => JSON.stringify(x, null, "  ");

const isStr = x => typeof x === "string";
const isNum = x => typeof x === "number";

function diff(a, b) {
  const fullOps = Diff.diffText(a, b);
  return fullOps.map(fullOp => {
    const [op, ai, bi, line] = fullOp;
    return h("p", { "class": `diff_line diff_${op}` }, line);
  });
}

function assertNonnull(x) {
  if (x == null) {
    throw new Error("should be non-null");
  }
}

function assert(x, msg="") {
  if (!x) {
    throw new Error("assertion failed: " + msg);
  }
}

function chomp(s) {
  if (s.endsWith("\n")) {
    return s.substring(0, s.length - 1);
  } else {
    return s;
  }
}

function indent(text) {
  return chomp(text).split("\n")
    .map(line => "    " + line)
    .join("\n")
}

const h = (type, attrs, ...kids) => new VNode(type, attrs || {}, ...kids);

// --------------------------------

/** abstract class */
class VNodeBase {
  constructor(type, cls) {
    assertNonnull(type);
    assertNonnull(cls);

    this.type = type;
    this._class = cls;
  }

  static getType(self) {
    return self == null ? "null" : self.type;
  }

  getKids() {
    throw new Error("implement in sub class");
  }
  renderTypeEq(type) {
    throw new Error("implement in sub class");
  }
  toFlat() {
    throw new Error("implement in sub class");
  }

  appendTo = parent => parent.appendChild(this.toReal())

  insertToVNode(parent, i) {
    parent.kids.splice(i, 0, this);
  }

  insertToNode(parent, i) {
    parent.insertBefore(
      this.toReal(),
      parent.childNodes[i]
    );
  }

  // --------------------------------

  static createBlank(type) {
    if (type === "__str__") {
      return new StrVNode("dummy");
    } else if (type === "__raw_html__") {
      return new RawHtmlVNode("dummy");
    } else {
      return new VNode(type);
    }
  }
}

class VNode extends VNodeBase {
  constructor(type, props, ... kids) {
    super(type, VNode);

    props = props || {};
    kids = kids || [];

    this.id = VNode.newId();
    this.props = props;
    this.eventListeners = [];

    // TODO Use factory
    this.kids = [];
    kids
      .filter(kid => kid != null)
      .forEach(kid => {
        if (Array.isArray(kid)) {
          kid.forEach(kidKid => {
            this.kids.push(this.convertKid(kidKid));
          });
        } else {
          this.kids.push(this.convertKid(kid));
        }
      });

    // TODO Use flatMap
    // this.kids =
    //   kids.flatMap(it => {
    //     if (it == null) {
    //       return [];
    //     } else if (Array.isArray(it)) {
    //       return it.map(kid => this.convertKid(kid));
    //     } else {
    //       return [this.convertKid(it)];
    //     }
    //   });
  }

  static classEq = x => x._class === VNode

  convertKid(kid) {
    if (isNum(kid)) {
      return new StrVNode(String(kid));
    } else if (isStr(kid)) {
      return new StrVNode(kid);
    } else {
      return kid;
    }
  }

  static newId() {
    if (VNode._id == null) {
      VNode._id = 0;
    }
    VNode._id++;
    return VNode._id;
  }

  getKids = () => this.kids

  toReal() {
    const el = document.createElement(this.type);
    const nodeId = VNode.newId();

    this._class.updateProps(el, {}, this.props, null, this);
    el.dataset.nodeid = `${nodeId}`; // for debug

    this.kids.forEach(kid => kid.appendTo(el));

    return el;
  }

  static updateProps(el, props0, props1, vnOld, vnNew) {
    // なくなった項目を除去
    const ks1 = Object.keys(props1);
    const ksToDelete = Object.keys(props0).filter(k0 => !ks1.includes(k0));
    ksToDelete.forEach(k0 => el.removeAttribute(k0));

    if (vnOld) {
      vnOld.eventListeners
        .forEach(pair => el.removeEventListener(...pair));
    }

    for (let [k, v] of Object.entries(props1)) {
      let m;
      if (m = k.match(/^on(.+)$/)) {
        const evName = m[1];
        el.addEventListener(evName, v);
        vnNew.eventListeners.push([evName, v]);
      } else if (k === "readonly" || k === "disabled") {
        if (!!v) {
          el.setAttribute(k, k);
        }
      } else if (k === "style") {
        for (let [sk, sv] of Object.entries(v)) {
          el.style[sk] = sv;
        }
      } else {
        el.setAttribute(k, v);
      }
    }
  }

  toPlain = () => (
    {
      type: this.type,
      props: this.props,
      kids: this.kids
    }
  )

  typeEq(other) {
    if (other == null) {
      return false;
    }

    return this.type === other.type;
  }

  // NOTE JSON.stringify だと function が消える
  static propsChanged(vnOld, vnNew) {
    assert(VNode.classEq(vnOld));
    assert(VNode.classEq(vnNew));
    // return tojson(vnOld.props) === tojson(vnNew.props); // TODO
    return true;
  }

  static getKey(self) {
    if (self.props) {
      return self.props.key || null;
    }
    return null;
  }

  toDebugTree() {
    let s = "";
    s += `(${this.type})`;
    s += `\n`;
    s += indent(`props: ` + JSON.stringify(this.props));
    s += `\n`;
    (this.kids || []).forEach(kid => {
      if (VNode.classEq(kid) || StrVNode.classEq(kid) || RawHtmlVNode.classEq(kid)) {
        // ok
      } else {
        puts(393, this, kid);
        throw new Error("invalid class");
      }
      s += indent("kid: " + kid.toDebugTree());
      s += `\n`;
    });
    return s;
  }

  renderTypeEq = type => this.props.__render_type__ === type

  removeChildAt(i) {
    this.kids.splice(i, 1);

    // flatMap も使える
    // this.kids = this.kids.flatMap((kid, ki) => ki === i ? [] : kid);
  }

  getNumKids = () => this.getKids().length

  toFlat() {
    const xs = [];
    xs.push(this.type);
    for (const [k, v] of Object.entries(this.props)) {
      if (typeof v === "function") {
        xs.push(this.type + "." + k + ":" + "(fn)");
      } else {
        xs.push(this.type + "." + k + ":" + v);
      }
    }
    this.getKids()
      .forEach(kid => {
        kid.toFlat()
          .forEach(kidFlat =>{
            xs.push(this.type + " / " + kidFlat);
          });
      });
    return xs;
  }
}

class StrVNode extends VNodeBase {
  constructor(str) {
    super("__str__", StrVNode);
    this.str = str;
  }

  static classEq = x => x._class === StrVNode

  toReal = () => document.createTextNode(this.str)
  toDebugTree = () => JSON.stringify(this.str)
  getKids = () => [];
  renderTypeEq = (_) => false
  toFlat = () => ["str:" + JSON.stringify(this.str)]
}

class RawHtmlVNode extends VNodeBase {
  constructor(html) {
    super("__raw_html__", RawHtmlVNode);
    assertNonnull(html);
    this.html = html;
  }

  static classEq = x => x._class === RawHtmlVNode

  toDebugTree = () => JSON.stringify(this.html)

  renderTypeEq = _ => false

  // diff の数合わせのため適当な内容で返す
  getKids = () => ["dummy"]

  toReal() {
    const el = document.createElement("div");
    el.classList.add("raw_html");
    el.innerHTML = this.html;
    assert(el.childNodes.length === 1);
    return el;
  }

  toFlat() {
    return ["raw_html:" + JSON.stringify(this.html)];
  }
}

// --------------------------------

class Vdom {
  constructor(render, sel) {
    this.render = render;
    this.mountPointSelector = sel;
  }

  init() {
    this.mountPoint = document.querySelector(this.mountPointSelector);
    this.vnPrev = h("span"); // dummy element
    // real dom と vnOld を同期させる
    this.mountPoint.appendChild(this.vnPrev.toReal());
  }

  // TODO rename / toDiffLine
  _diff_kids(vnode) {
    return vnode.getKids().map(kid => {
      let s = "";
      s += VNodeBase.getType(kid);
      s += ` // ` + VNode.getKey(kid); // TODO change sep
      return s;
    });
  }

  adjustTypes(parent, vnOld, vnNew) {
    _puts("==>> _diff", vnOld, vnNew);
    _puts("373",
         this._diff_kids(vnOld),
         this._diff_kids(vnNew)
        );

    const fullOps = Diff.diff(
      this._diff_kids(vnOld),
      this._diff_kids(vnNew)
    );

    _puts("261 fullOps", fullOps);
    fullOps.forEach((it, i) => _puts(`fullOps | ${i}`, ...it));

    let i0 = vnOld.getKids().length;
    _puts(254, fullOps.length - 1, fullOps);

    for (let i = fullOps.length - 1; 0 <= i; i--) {
      const [op, ai, bi, line] = fullOps[i];
      _puts(`op_line each | ${i} i0(${i0}) ai(${ai}) bi(${bi})`, op, line);
      switch (op) {
      case "nop":
        _puts(`${i} nop`);
        i0--;
        break;
      case "add":
        const type = line.split(" // ")[0];
        _puts(`${i} add | (${type})`, i0, [ai, bi]);

        const blankVNode = VNodeBase.createBlank(type);

        // vnode, real node 両方を同期させる
        blankVNode.insertToVNode(vnOld, i0);
        blankVNode.insertToNode(parent, i0);
        // _puts(406, parent, vnOld, blankVNode);
        _puts(288, this.dump(vnOld));
        break;
      case "del":
        const kid = parent.childNodes[ai];
        _puts(`${i} del | `, i0, [ai, bi], kid, parent);
        parent.removeChild(kid);
        vnOld.removeChildAt(ai);
        i0--;
        break;
      default:
        throw new Error(`invalid operator (${op})`);
      }
    }
  }

  updateDebugDiff(
    treeStr_a0, treeStr_a1,
    treeStr_b0, treeStr_b1
  ) {
    const debugBox = document.querySelector("#debug");
    if (debugBox == null) {
      return;
    }

    debugBox.innerHTML = "";
    debugBox.appendChild(
      h("div", {}
      , h("table", {}
        , h("tr", {}
          , h("td", { style: { "vertical-align": "top" } }
            , h("pre", {}, tojson(this.vnPrev.toPlain()))
            )
          , h("td", { style: { "vertical-align": "top" } }
            , h("pre", {}, ...(diff(treeStr_a0, treeStr_a1)))
            )
          , h("td", { style: { "vertical-align": "top" } }
            , h("pre", {}, ...(diff(treeStr_b0, treeStr_b1)))
            )
          )
        )
      ).toReal()
    );
  }

  // ren => diffAndPatch
  _patch(parent, vnOld, vnNew) {
    _puts("================================");
    _puts("33 parent", parent);
    // puts("38 vnOld", tojson(vnOld.toPlain()));
    // puts("39 vnNew", tojson(vnNew.toPlain()));
    _puts(`410 vnOld ` + this.dump(vnOld));
    _puts(`410 vnNew ` + this.dump(vnNew));

    // 要素数・type を揃える
    this.adjustTypes(parent, vnOld, vnNew);
    if (RawHtmlVNode.classEq(vnOld) && RawHtmlVNode.classEq(vnNew)) {
      if (
        vnOld.getKids().length !== vnNew.getKids().length
      ) {
        puts("parent", parent);
        puts("parent.childNodes", parent.childNodes.length, parent.childNodes);
        puts("vnOld.kids", vnOld.getKids().length, vnOld.getKids());
        puts("vnNew.kids", vnNew.getKids().length, vnNew.getKids());
        throw new Error("assertion failed");
      }
    } else {
      if (
        parent.childNodes.length !== vnOld.getKids().length
          || parent.childNodes.length !== vnNew.getKids().length
      ) {
        puts("parent", parent);
        puts("parent.childNodes", parent.childNodes.length, parent.childNodes);
        puts("vnOld.kids", vnOld.getKids().length, vnOld.getKids());
        puts("vnNew.kids", vnNew.getKids().length, vnNew.getKids());
        throw new Error("assertion failed");
      }
    }

    _puts("----");
    _puts("337 parent", parent);
    _puts(`428 vnOld ` + this.dump(vnOld));
    _puts(`428 vnNew ` + this.dump(vnNew));

    // props, kids を更新
    if (StrVNode.classEq(vnOld) && StrVNode.classEq(vnNew)) {
      // 両方 str
      if (vnOld.str !== vnNew.str) {
        parent.textContent = vnNew.str;
      }
    } else if (RawHtmlVNode.classEq(vnOld) && RawHtmlVNode.classEq(vnNew)) {
      // 両方 raw html
      parent.innerHTML = vnNew.html;
    } else if (VNode.classEq(vnOld) && VNode.classEq(vnNew)) {
      // 両方 vnode
      if (VNode.propsChanged(vnOld, vnNew)) {
        VNode.updateProps(parent, vnOld.props, vnNew.props, vnOld, vnNew);
      }

      const numKids = vnNew.getNumKids();

      for (let i = 0; i < numKids; i++) {
        _puts(350, i, numKids);
        const kidRealNode = parent.childNodes[i];
        const kidOld = vnOld.kids[i];
        const kidNew = vnNew.kids[i];
        _puts("446", kidOld);
        _puts("446", kidNew);

        if (kidNew.renderTypeEq("force")) {
          // 強制的に入れ替え
          parent.replaceChild(kidNew.toReal(), kidRealNode);
        } else {
          this._patch(kidRealNode, kidOld, kidNew);
        }
      }
    } else {
      throw new Error("should be same type");
    }
  }

  patch(state) {
    const vnOld = this.vnPrev;
    const vnNew = this.render(state);

    const treeStr_a0 = vnOld.toDebugTree();
    const treeStr_a1 = vnNew.toDebugTree();

    const treeStr_b0 = vnOld.toFlat().join("\n");
    const treeStr_b1 = vnNew.toFlat().join("\n");

    this._patch(
      this.mountPoint,
      h("dummy", {}, vnOld),
      h("dummy", {}, vnNew)
    );
    this.vnPrev = vnNew;

    try {
      this.updateDebugDiff(
        treeStr_a0, treeStr_a1,
        treeStr_b0, treeStr_b1
      );
    } catch(e) {
      puts(e);
    }
  }

  dump(x) {
    if (isStr(x)) {
      return `str (${x})`;
    }
    if (isNum(x)) {
      return `num (${x})`;
    }

    return JSON.stringify(x);
  }
}

class App {
  constructor(view, sel) {
    this.vdom = new Vdom(
      view,
      sel
    );
    this.initialized = false;
  }

  update = () => {
    if (!this.initialized) {
      this.vdom.init()
      this.initialized = true;
    }
    this.vdom.patch(this.state)
  };
}

window.VDom = { h, RawHtmlVNode, App };

})();
