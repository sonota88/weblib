function debounce(name, fn, msec) {
  return ()=>{
    if (name in debounce.timerMap) {
      clearTimeout(debounce.timerMap[name]);
    }
    debounce.timerMap[name] = setTimeout(fn, msec);
  };
}

debounce.timerMap = {};
