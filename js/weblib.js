class Weblib {

  static parseInt(x) {
    return Math.trunc(x);
  }

  /**
   * TODO Use toSorted
   * https://speakerdeck.com/line_developers/how-your-familiar-javascript-coding-techniques-will-change-in-the-near-future?slide=17
   */
  static nonDestructiveSort(xs) {
    return [...xs].sort();
  }

}
