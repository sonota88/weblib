(()=>{

const puts = (...args) => console.log(...args);
const _puts = () => void 0;

class Diff {
  constructor(lsa, lsb) {
    this.xs_a = [null].concat(lsa);
    this.xs_b = [null].concat(lsb);
    this.max_cost = (this.xs_a.length - 1)
      + (this.xs_b.length - 1);

    this.grid = [];
    for (let ai = 0; ai < this.xs_a.length; ai++) {
      this.grid[ai] = [];
    }
  }

  static diff(a, b) {
    const d = new Diff(a, b);
    d.fill();
    _puts("[diff edit graph] ", d.grid);
    const ops = d.walk();
    return d.toFullOps(ops);
  }

  static diffText(a, b) {
    const d = new Diff(
      a.split("\n"),
      b.split("\n")
    );
    d.fill();
    const ops = d.walk();
    return d.toFullOps(ops);
  }

  set(ai, bi, c) {
    this.grid[ai][bi] = c;
  }

  get(ai, bi) {
    if (ai < 0) { return null; }
    if (bi < 0) { return null; }

    return this.grid[ai][bi];
  }

  nop_p(ai, bi) {
    return this.xs_a[ai] === this.xs_b[bi];
  }

  getCost(ai, bi) {
    if (this.nop_p(ai, bi)) {
      return this.get(ai - 1, bi - 1);
    } else {
      return Math.min(
        this.get(ai - 1, bi    ),
        this.get(ai    , bi - 1)
      ) + 1
    }
  }

  fill() {
    const len_a = this.xs_a.length;
    const len_b = this.xs_b.length;
    for (let ai = 0; ai < len_a; ai++) {
      this.set(ai, 0, ai);
    }
    for (let bi = 0; bi < len_b; bi++) {
      this.set(0, bi, bi);
    }
    for (let ai = 1; ai < len_a; ai++) {
      for (let bi = 1; bi < len_b; bi++) {
        this.set(ai, bi, this.getCost(ai, bi));
      }
    }
  }

  _walk_op(ai, bi) {
    const c_del = this.get(ai - 1, bi);
    const c_add = this.get(ai    , bi - 1);
    const c_nop = this.nop_p(ai, bi)
          ? this.get(ai - 1, bi - 1)
          : Number.POSITIVE_INFINITY
          ;

    const min = Math.min(
      c_del == null ? Number.POSITIVE_INFINITY : c_del,
      c_add == null ? Number.POSITIVE_INFINITY : c_add,
      c_nop
    );

    if      (min === c_nop) { return "nop"; }
    else if (min === c_add) { return "add"; }
    else if (min === c_del) { return "del"; }
    else {
      throw new Error(
        "invalid operation:"
          + ` min(${min})`
          + ` c_nop(${c_nop}) c_add(${c_add}) c_del(${c_del})`
      );
    }
  }

  _walk_pos(op, ai, bi) {
    switch (op) {
    case "nop": return [ai - 1, bi - 1];
    case "add": return [ai    , bi - 1];
    case "del": return [ai - 1, bi    ];
    default:
      throw new Error("invalid operation");
    }
  }

  walk() {
    let ai = this.xs_a.length - 1;
    let bi = this.xs_b.length - 1;
    const ops = [];

    while (0 < ai || 0 < bi) {
      const op = this._walk_op(ai, bi);
      [ai, bi] = this._walk_pos(op, ai, bi);
      ops.unshift(op);
    }

    return ops;
  }

  toFullOps(ops) {
    let ai = 0;
    let bi = 0;

    let fullOps = [];
    ops.forEach(op => {
      let line, _ai, _bi;

      switch (op) {
      case "del":
        line = this.xs_a[ai + 1];
        _ai = ai;
        ai++;
        break;
      case "add":
        line = this.xs_b[bi + 1];
        _bi = bi;
        bi++;
        break;
      case "nop":
        line = this.xs_a[ai + 1];
        _ai = ai;
        _bi = bi;
        ai++;
        bi++;
        break;
      default:
        throw new Error("invalid operation");
      }
      fullOps.push([op, _ai, _bi, line]);
    });

    return fullOps;
  }

}

window.Diff = Diff;

})();
