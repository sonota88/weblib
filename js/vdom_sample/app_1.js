const puts = (...args) => console.log(...args);
const _puts = () => void 0;

function debounce(name, fn, msec) {
  return (...args)=>{
    if (name in debounce.timerMap) {
      clearTimeout(debounce.timerMap[name]);
    }
    debounce.timerMap[name] = setTimeout(()=>{ fn(...args) }, msec);
  };
}
debounce.timerMap = {};

// --------------------------------

const h = VDom.h;

class TextareaBox {
  static render(state) {
    function getList(_state) {
      try {
        return JSON.parse(_state.text);
      } catch(e) {
        return [];
      }
    }

    function ok_p(_state) {
      try {
        JSON.parse(_state.text);
        return true;
      } catch(e) {
        return false;
      }
    }

    return (
      h("div", {}

      , getList(state).map(x =>
          h("p", {}, x)
        )
      , h("textarea", {
            __render_type__: "uncontrolled"
          , oninput: debounce("oninput_text", (ev)=>{ __p.oninput_text(ev); }, 200)
          , key: 1
          }
        , state.text
        )

      , h("p", {
            style: {
              background: ok_p(state) ? "#8f8" : "#f88"
            }
          , "class": ok_p(state) ? "ok" : "ng"
          }
        , ok_p(state) ? "OK" : "NG"
        )
      // , h("ul", {}
      //   , getList(state).map(x =>
      //       h("li", {}, x)
      //     )
      //   )

      )
    );
  }
}

class App extends VDom.App {
  constructor(sel) {
    super(App.render.bind(App), sel);

    this.state = {
      text: "[1,2,3]",
      n: 0,
      xs: [
        { key: 1, text: "x1" }
      ]
    };
  }

  static render(state) {
    const checkboxAttrs = {
      type: "checkbox"
    };
    if (state.n % 2 === 1) {
      checkboxAttrs.checked = "checked";
    }

    return (
      h("div", {}

      , h("button", {
            onclick: ()=>{ __p.onclick_increment(); }
          }
        , "increment"
        )
      , h("span", {
            __render_type__: "force"
          }
        , state.n
        )
      , h("input", checkboxAttrs)

      , state.n % 2 == 0
        ? h("button", {}, "even")
        : h("span", {}, "odd")

      , TextareaBox.render(state)

      , h("hr")
      , h("input", {
            onchange: (ev)=>{ __p.onchange_input(ev); }
          }
        )
      , h("input", {
            value: state.n
          }
        )
      , h("hr")
      , h("ul", {}
        , state.xs.map(x =>
            h("li", {}, x.text)
          )
        )

      , new VDom.RawHtmlVNode(`<span style="color: red;">raw html</span>`)

      )
    );
  }

  onclick_increment() {
    this.state.n++;
    this.update();
  }

  oninput_text(ev) {
    this.state.text = ev.target.value;
    this.update();
  }

  onchange_input(ev) {
    this.state.n++;
    this.update();
  }
}

function init() {
  window.__p = new App("#mount_point");
  __p.update();
}

document.addEventListener("DOMContentLoaded", init);
